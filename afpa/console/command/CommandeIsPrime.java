package afpa.console.command;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.exception.ParametresIncorrectesException;

public class CommandeIsPrime extends AbstractCommandeAvecParam implements IHistoriqueCommand {

	public static final String CMD = "isprime";
	private static final String DESC = "D�termine si l'entier en param�tre est premier.";

	public CommandeIsPrime(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}
	

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		
		String[] params = this.parameter.split(" ");
		
		try {
			
			if(params.length != 1) {
				throw new Exception();
			}
			
			long l = Long.parseLong(params[0]);
			
			for(long i = 2 ; i < l ; i++) {
				if(l%i == 0) {
					System.out.println("Le nombre " + l + " n'est pas premier.");
					return;
				}
			}
			
			System.out.println("Le nombre " + l + " est premier.");
			
		} catch(Exception e) {
			throw new ParametresIncorrectesException("La commande isprime attends 1 entier en parametre");
		}

	}

}
