package afpa.console.command.criteres;

class CritereEqual extends Critere {

	public CritereEqual(String s) {
		super(s);
	}

	@Override
	public boolean test(String s) {
		System.err.println(param);
		return s.equals(param);
	}

}
