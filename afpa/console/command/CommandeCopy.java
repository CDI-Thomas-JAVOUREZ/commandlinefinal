package afpa.console.command;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeCopy extends AbstractCommandeAvecParam implements IHistoriqueCommand {
	
	public static final String CMD = "copy";
	private static final String DESC = "Copie le fichier en premier param�tre dans le dossier de destination";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}


	public CommandeCopy(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		
		File origin = null;
		File destination = null;
		
		String[] arguments = this.parameter.split(" ");
		
		try {
		if(arguments.length != 2) {
			throw new Exception("Nombre d'arguments invalide"); 
		}
		
		origin = Paths.get(CommandeFactory.CURRENT_FILE.toString(), arguments[0]).toFile();
		
		if(!origin.exists()) {
			throw new Exception("Le fichier d'origine n'existe pas.");
		}
		
		if(origin.isDirectory()) {
			throw new Exception("L'origine est un repertoire");
		}
		
		if(!origin.canWrite()) {
			throw new Exception("Copie interdite pour ce fichier");
		}
		
		destination = Paths.get(arguments[1],arguments[0]).toFile();

		if(!destination.getParentFile().exists() || !destination.getParentFile().isDirectory()) {
			throw new Exception("La destination n'est pas un r�pertoire, ou n'existe pas.");
		}
			
		if(destination.exists()) {
			throw new Exception("Le fichier existe d�j� dans le repertoire de destination.");
		}
		
		if(!destination.getParentFile().canWrite()) {
			throw new Exception("Ecriture impossible dans ce repertoire");
		}
			
		Files.copy(origin.toPath(), destination.toPath());
		System.out.println("Le fichier " + origin + " a ete copie vers " + destination);
		
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}

}
