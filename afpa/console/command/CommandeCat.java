package afpa.console.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeCat extends AbstractCommandeAvecParam implements IHistoriqueCommand {
	
	public static final String CMD = "cat";
	private static final String DESC = "Affiche le contenu d'un fichier.";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeCat(String c) {
		super(c, CMD);
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		
		try{
			String[] arguments = this.parameter.split(" ");
			
			if(arguments.length != 1) {
				throw new Exception("Nombre d'arguments incorrects");
			}
			
			File f = Paths.get(CommandeFactory.CURRENT_FILE.toString(),arguments[0]).toFile();
			
			if(!f.exists()) {
				throw new Exception("Le fichier n'existe pas");
			}
			
			if(f.isDirectory()) {
				throw new Exception("Il s'agit d'un repertoire");
			}
			
			if(!f.canRead()) {
				throw new Exception("Vous ne disposez pas des droits de lecture sur ce fichier");
			}
	
			StringBuilder res = new StringBuilder();
		
			InputStream flux=new FileInputStream(f); 
			InputStreamReader lecture=new InputStreamReader(flux);
			BufferedReader buff=new BufferedReader(lecture);
			
			String ligne;
			while ((ligne=buff.readLine())!=null){
				res.append(ligne).append("\n");
			}
			buff.close(); 
			
			System.out.println(res.toString());
			
		}		
			catch (Exception e){
			System.err.println(e.getMessage());
		}
		

	}

}
