package afpa.console.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.exception.OptionsIncorrectesException;

public class CommandeFline extends AbstractCommandeAvecParam implements IHistoriqueCommand {
	
	public static final String CMD = "fline";
	private static final String DESC = "Lit un fichier nomm� en param�tre en ligne";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeFline(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void run() throws CommandException {
		// TODO Auto-generated method stub

		try {
			
			String searched = "";
			long numLine = 0;
			boolean count = false;
			
			long begin = 0;
			long end = Long.MAX_VALUE;
			
			String file;
			if(this.parameter.indexOf(" ") != -1) {
				file = this.parameter.substring(0, this.parameter.indexOf(" "));
			} else {
				file = this.parameter;
			}
			
			
			File f = Paths.get(CommandeFactory.CURRENT_FILE.toString(),file).toFile();
			
			if(!f.exists()) {
				throw new Exception("Le fichier n'existe pas");
			}
			
			if(f.isDirectory()) {
				throw new Exception("Il s'agit d'un repertoire");
			}
			
			if(!f.canRead()) {
				throw new Exception("Vous ne disposez pas des droits de lecture sur ce fichier");
			}
			
			String[] arguments = null;
			
			if(! file.equals(this.parameter)) {
				arguments = this.parameter.substring(this.parameter.indexOf(" ",0)+1).split(" ");
			} else {
				arguments = new String[1];
				arguments[0] = "all";
			}
			
			for(String argument : arguments) {
				
				if(argument.startsWith("all")) {
					break;
				}
				
				else if(argument.startsWith("-n")) {
					count = true;
					continue;
				}
				
				else if(argument.startsWith("-s")) {
					searched = argument.substring(2);
					continue;
				}
					
				else if(argument.startsWith("-d")) {
					begin = Long.parseLong(argument.substring(2));
					continue;
				}
				else if(argument.startsWith("-f")) {
					end = Long.parseLong(argument.substring(2));
				}
				else {
					throw new OptionsIncorrectesException("Argument " + argument + " invalide.");
				}
			}
	
			StringBuilder res = new StringBuilder();

			BufferedReader buff=new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			
			String ligne;
			
			while ((ligne=buff.readLine())!=null){
				numLine++;
				
				if(numLine >= begin && numLine <= end && !count && (!searched.equals("") ? ligne.contains(searched) : true)) {
					res.append(ligne).append("\n");
				}
			}
			buff.close(); 
			
			if(count) { 
				res.append("Nombre de lignes : " + numLine);
			}
			
			System.out.println(res.toString());
		
	}		
		catch (Exception e){
		System.err.println(e.getMessage());
	}
		

	}

}
