package afpa.console.command;

import java.io.File;
import java.nio.file.Paths;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeCRD extends AbstractCommandeAvecParam implements IHistoriqueCommand {
	
	public static final String CMD = "crd";
	private static final String DESC = "Cr�e le r�pertoire nomm� en premier param�tre dans le dossier actuel";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}


	public CommandeCRD(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		try {
			
			if(!CommandeFactory.CURRENT_FILE.canWrite()) {
				throw new CommandException("Vous n'avez pas les droits pour cette action");
			}
			
			File f= Paths.get(CommandeFactory.CURRENT_FILE.toString(),this.parameter).toFile(); 
			
			if(f.exists()) {
				throw new CommandException("Le fichier existe deja");
			}
			
			f.mkdir();
			System.out.println("Le repertoire " + f.getName() + " a ete cree.");
		
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}

}
