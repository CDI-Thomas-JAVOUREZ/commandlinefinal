package afpa.console.command;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

import afpa.console.command.interfaces.ICommand;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.hist.EntreeCmdHist;

public class CommandeFactory {

	public static File CURRENT_FILE = new File(System.getProperty("cdi.default.folder") == null ? System.getProperty("user.dir") : System.getProperty("cdi.default.folder"));

	private static final List<EntreeCmdHist> COMMANDES_HIST_LIST = new ArrayList<>();

	private static final Map<String, String> COMMANDES_LIST_DESC = new HashMap<>();
	
	static {
		CommandeRiver.chargerStaticPortion();
		CommandeCd.chargerStaticPortion();
		CommandeHelp.chargerStaticPortion();
		CommandeDir.chargerStaticPortion();
		CommandeDirng.chargerStaticPortion();
		CommandeExit.chargerStaticPortion();
		CommandeFin.chargerStaticPortion();
		CommandeHist.chargerStaticPortion();
		CommandeHistclear.chargerStaticPortion();
		CommandePwd.chargerStaticPortion();
		CommandeQuit.chargerStaticPortion();
		CommandeIsPrime.chargerStaticPortion();        
		CommandeCat.chargerStaticPortion(); 
		CommandeCopy.chargerStaticPortion();
		CommandeCRF.chargerStaticPortion();
		CommandeCRD.chargerStaticPortion(); 
		CommandeGetVars.chargerStaticPortion();
		CommandeFline.chargerStaticPortion();
		CommandeCount.chargerStaticPortion();
	}

	public static void addCommandeDesc(String cmd, String desc) {
		COMMANDES_LIST_DESC.put(cmd, desc);
	}

	public static ICommand create(String cmd) {
		ICommand theCommand = null;

		if (cmd.length() == 0) {
			theCommand = new CommandeVide();

		} else if (CommandePwd.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandePwd();

		} else if (CommandeHist.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeHist(COMMANDES_HIST_LIST);

		} else if (CommandeExit.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeExit();

		} else if (CommandeDir.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeDir();

		} else if (cmd.toLowerCase().startsWith(CommandeCd.CMD + " ")) {
			theCommand = new CommandeCd(cmd);
		
		} else if (cmd.toLowerCase().startsWith(CommandeRiver.CMD + " ")) {
			theCommand = new CommandeRiver(cmd);
			
		} else if (cmd.toLowerCase().startsWith(CommandeIsPrime.CMD + " ")) {
			theCommand = new CommandeIsPrime(cmd);
			
		} else if (cmd.toLowerCase().startsWith(CommandeCat.CMD + " ")) {
			theCommand = new CommandeCat(cmd);
			
		} else if (cmd.toLowerCase().startsWith(CommandeCopy.CMD + " ")) {
			theCommand = new CommandeCopy(cmd);
			
		} else if (cmd.toLowerCase().startsWith(CommandeCRF.CMD + " ")) {
			theCommand = new CommandeCRF(cmd);
			
		} else if (cmd.toLowerCase().startsWith(CommandeCRD.CMD + " ")) {
			theCommand = new CommandeCRD(cmd);
			
		} else if (CommandeGetVars.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeGetVars(CommandeGetVars.CMD + " all");
			
		} else if (cmd.toLowerCase().startsWith(CommandeGetVars.CMD + " ")) {
			theCommand = new CommandeGetVars(cmd);
			
		} else if (cmd.toLowerCase().startsWith(CommandeFline.CMD + " ")) {
			theCommand = new CommandeFline(cmd);

		} else if (CommandeFin.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeFin();

		} else if (CommandeHistclear.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeHistclear(COMMANDES_HIST_LIST);

		} else if (CommandeDirng.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeDirng();

		} else if (CommandeHelp.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeHelp();

		} else if (CommandeQuit.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeQuit();

		} else if (cmd.toLowerCase().startsWith(CommandeFind.CMD + " ")) {
			theCommand = new CommandeFind(cmd);
			
		} else if (CommandeCount.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeCount(cmd + " -t 0");

		} else if (cmd.toLowerCase().startsWith(CommandeCount.CMD + " ")) {
			theCommand = new CommandeCount(cmd);

		} else {
			theCommand = new CommandeIntrouvable();
		}

		if (theCommand != null && (theCommand instanceof IHistoriqueCommand)) {
			COMMANDES_HIST_LIST.add(new EntreeCmdHist(cmd));
		}

		return theCommand;
	}

	public static Map<String, String> getCommandesList() {
		return new HashMap<>(COMMANDES_LIST_DESC);
	}

}
