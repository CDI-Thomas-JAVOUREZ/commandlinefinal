package afpa.console.command;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.exception.OptionsIncorrectesException;

/*ajouter la commande "count" qui calcule le nombre de fichiers et dossiers dans le repertoire en cours
et ses sous repertoires , sans multithread :
    > count
    100 fichiers
    50    dossiers
    > count -f
    100 fichiers
    > count -d
    50 dossiers
    > count -df
    100 fichiers
    50    dossiers
    > count -fd
    100 fichiers
    50    dossiers
    > count -d -f
    100 fichiers
    50    dossiers
    > count -k
    unknown option -r
    > count -fy
    unknown option -y

- ajouter l'option -p qui permet de chercher les fichiers ou dossier dont le nom
correspond � la regex pass�e en param�tre de l'option
    > count -p ^d.*\.pdf -f
    5 fichiers
    > count -p ^d.*\.pdf -d
    10 dossiers
    > count -p ^d.*\.pdf
    5 fichiers
    10 dossiers

- ajouter l'option -t qui prends une valeur:
-> si le param�tre -t 0 est pass�, la version mono thread est utilis�e.

-> si le parametre -t 1 est pass� en parametre une version avec
une implementation de Thread ou Runnable est mise en place.

-> si le parametre -t 2 est pass� en parametre une version avec
une implementation utilsant les Executor et les Runnable.

-> si le parametre -t 3 est est pass� en parametre une version avec
une implementation utilisant les Callable et les Future.

-> si pas de parametre t alors la commande est lanc�e en monothtread (sans monothread)
comme avec le parametre -t 0.
les options -p et -t ne peuvent pas �tre rassembl�es avec d'autres options.
 */


public class CommandeCount  extends AbstractCommandeAvecParam implements IHistoriqueCommand {

	public static final String CMD = "count";
	private static final String DESC = "Compte le nombre de fichiers et de dossiers.";

	private byte threadOption = 0;
	private Pattern pattern = null;
	private byte displayOption = 0;

	DirectoryStream.Filter<Path> filter = new DirectoryStream.Filter<Path>() {
		public boolean accept(Path x) throws IOException {
			return x.toFile().getName().matches(pattern.toString());
		}
	};

	AtomicLong nbFiles = new AtomicLong(0);
	AtomicLong nbFolders = new AtomicLong(0);

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	private class CountRunnable implements Runnable {

		private File f;
		private Semaphore sem;

		CountRunnable(File f, Semaphore sem) {
			this.f = f;
			this.sem = sem;
		}

		@Override
		public void run() {

			try (DirectoryStream<Path> stream = Files.newDirectoryStream(f.toPath(), filter)) {

				sem.acquire();

				for (Path entry: stream) {
					if(entry.toFile().isFile()) {
						nbFiles.getAndIncrement();
					}
					else {
						nbFolders.getAndIncrement();
						Thread tmp = new Thread(new CountRunnable(f,sem));
						tmp.start();
						tmp.join();
					}
				}

				sem.release();

			} catch (DirectoryIteratorException ex) {
				System.err.println(ex.getMessage());
			} catch (IOException e) {

			} catch (InterruptedException e) {
				System.err.println("Thread interrompu");
			}

		}

	}


	public static void chargerStaticPortion() {
	}

	public CommandeCount(String c) {
		super(c, CMD);
	}

	@Override
	public void run() throws CommandException {

		try {
			setParam();

			if(pattern == null) {
				pattern=Pattern.compile(".*");
			}

			switch(threadOption) {
			case 0: count(CommandeFactory.CURRENT_FILE.toPath()); break;

			case 1: {
				Thread t = new Thread(new CountRunnable(CommandeFactory.CURRENT_FILE,
						new Semaphore(Runtime.getRuntime().availableProcessors()))); 
				t.start();
				t.join();
				break;
			}

			case 2: {

				ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

				countExecutor(executor, CommandeFactory.CURRENT_FILE); 

				break;
			}

			case 3: countCallable(); break;

			default: count(CommandeFactory.CURRENT_FILE.toPath());
			
			}
			
			System.out.print(displayOption != 2 ? nbFiles + " Fichiers trouv�s\n" : "");
			System.out.print(displayOption != 1 ? nbFolders + " Dossiers trouv�s\n" : "\n");
			
		} catch (CommandException e) {
			System.err.println(e.getMessage());
		} catch (InterruptedException e) {
			System.err.println("Thread interrompu");
		}
		
	}

	private void count(Path dir) {

		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, filter)) {
			

			for (Path entry: stream) {
				if(entry.toFile().isFile()) {
					nbFiles.getAndIncrement();
				}
				else {
					nbFolders.getAndIncrement();
					count(entry);
				}
			}
		} catch (DirectoryIteratorException ex) {
			System.out.println(ex.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}


	private void countExecutor(ExecutorService executor, File directory) {


		final List<File> child = Arrays.asList(directory.listFiles());

		Runnable r1 = ()-> {
			Consumer<File> consumer = (File x)-> { 
				if(x.isFile() && x.getName().matches(pattern.toString())) {
					nbFiles.getAndIncrement();
				}

				if(x.isDirectory()) {
					
					 if(x.getName().matches(pattern.toString())) {
					nbFolders.getAndIncrement();
					 }
					 
					countExecutor(executor, x);
				}
			};
			child.forEach(consumer);
		};

		executor.submit(r1);

	}


	private void countCallable() {

	}

	private void setParam()
			throws CommandException {
		String[] options = this.parameter.split(" ");

		if(options.length == 0) {
			return;
		}

		for(int i = 0; i<options.length;i++) {

			boolean optionUnknown = true;

			if("-t".equals(options[i])) {
				if(options.length > i) {
					optionUnknown = false;
					break;
				} else {
					if("1".equals(options[i+1]) || "2".equals(options[i+1]) ||  "3".equals(options[i+1])) {
						threadOption = Byte.parseByte(options[i+1]);
						i++;
						optionUnknown = false;
						break;
					}
					else {
						throw new OptionsIncorrectesException("Option " + options[i+1] + " non reconnue pour le parametre -t");
					}
				}
			}

			if("-p".equals(options[i])) {

				if(options.length < i+1) {
					throw new OptionsIncorrectesException("Pattern non d�fini");
				}

				try {
					pattern = Pattern.compile(options[i+1]);
					i++;
				} catch(PatternSyntaxException e) {
					throw new OptionsIncorrectesException("Pattern incorrect");
				}
				optionUnknown = false;
				break;

			}

			if(options[i].startsWith("-")){
				for(int j = 1;j < options[i].length() ; j++) {
					switch(options[i].charAt(j)) {
					case 'd': {
						displayOption += 2;
						optionUnknown = false;
						break;
					}
					case 'f': {
						displayOption +=1;
						optionUnknown = false;
						break;
					}				
					case 'p':
					case 't': {
						throw new OptionsIncorrectesException("L'option -"+ options[i].charAt(j) + " ne peut pas etre rassembl�e avec les autres.");
					}
					default: throw new OptionsIncorrectesException("Option -" + options[i].charAt(j) + " non reconnue");
					}
				}
			}

			if(optionUnknown)
				throw new OptionsIncorrectesException("Option " + options[i] + " non reconnue" );
		}

	}

	@Override
	public String getName() {

		return CMD;
	}

}
