package afpa.console.command;

import java.io.File;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Map.Entry;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.exception.OptionsIncorrectesException;

public class CommandeGetVars extends AbstractCommandeAvecParam implements IHistoriqueCommand {
	
	public static final String CMD = "getvars";
	private static final String DESC = "Affiche les variables d'environnement, et les propriétés de la JVM";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}


	public CommandeGetVars(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		
		if(this.parameter.equals("all")) {
			System.out.println(getEnv() + getProp());
			return;
		}
		
		if(this.parameter.equals("-env")) {
			System.out.println(getEnv());
			return;
		}
		
		if(this.parameter.equals("-prop")) {
			System.out.println(getProp());
			return;
		}
		
		throw new OptionsIncorrectesException("");

	}
	
	private String getEnv() {
		
		StringBuilder res = new StringBuilder();
		
		for(Map.Entry<String, String> v : System.getenv().entrySet()) {
			res.append(v.getKey() + " : "  + v.getValue() + "\n");
		}
		
		return res.toString()+"\n";
	}
	
	private String getProp() {
		
		StringBuilder res = new StringBuilder();
		
		for(Entry<Object, Object> p : System.getProperties().entrySet()) {
			res.append(p.getKey() + " : " + p.getValue() + "\n");
		}
		
		return res.toString();
	}

}
