package afpa.console.command;

import afpa.console.exception.CommandException;
import afpa.console.exception.ParametresIncorrectesException;

public class CommandeRiver extends AbstractCommandeAvecParam {
	
	public static final String CMD = "river";
	private static final String DESC = "Calcule le point de convergence des 2 rivi�res port�es par les nombres en param�tres";

	private static final int ITERMAX = 1000;
	
	long r1;
	long r2;
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}
	
	public CommandeRiver(String c) {

		super(c,CMD);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		
		String[] params = this.parameter.split(" ");
		
		try {
			
			if(params.length != 2) {
				throw new Exception();
			}
			
			r1 = Long.parseLong(params[0]);
			r2 = Long.parseLong(params[1]);
			
			System.out.println(riverResult(r1,r2));
			
		} catch(Exception e) {
			throw new ParametresIncorrectesException("La commande River attends 2 entiers en parametre");
		}
		
	}
	
	private static String riverResult(long r1,long r2) {
        
		int iter = 0;
		
        while(r1 != r2) {
        	
            if(r1>r2){
                r2 = RiverNext(r2);
            }
            else{
                r1 = RiverNext(r1);
            }
            
            iter++;
            
            if(iter >= ITERMAX) {
            	return "Il n'y a pas de point de convergence trouv�.";
            }
        }


       return "Le point de convergence entre les 2 suites est situ� � " + r1;
    }
    
    
    public static long RiverNext(long r) {
    
    long result = r;
    
    do {
        result += r%10;
        r = r/10;
    }while(r/10 > 0);
    
    result += r%10;
    
    return result;
    
    }

}
